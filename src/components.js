Crafty.c('Grid', {
    init: function() {
        this.attr({
            w: Game.tile_grid.tile.width,
            h: Game.tile_grid.tile.height
        });
    },

    // Locate this entity at the given position on the grid
    at: function(x,y) {
        /*if (x === undefined && y === undefined) {
            return { x: this.x/Game.tile_grid.tile.width, y: this.y/Game.tile_grid.tile.height }
        }
        else {
            this.attr({ x: x * Game.tile_grid.tile.width, y: y * Game.tile_grid.tile.height });
            return this;
        }*/
    }
});

Crafty.c('Actor', {
    required: '2D, Canvas, Grid'
});

Crafty.c('Tile', {
    required: 'Actor, Mouse'
});

/*Crafty.c('Grass', {
    required: 'Tile, spr_grass'
});*/

Crafty.c('Stone', {
    required: 'Tile, spr_stone'
});

Crafty.c('Tree', {
    required: 'Actor, Solid, spr_tree'
});

Crafty.c('PlayerCharacter', {
    required: 'Actor, spr_player',

    init: function() {

    }
});
