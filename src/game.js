
Game = {
    tile_grid: {
        width: 8,
        height: 8,
        tile: {
            width: 128,
            height: 128
        }
    },

    width: function() {
        return this.tile_grid.width * this.tile_grid.tile.width;
    },

    height: function() {
        return this.tile_grid.height * this.tile_grid.tile.height;
    },

    start: function() {
        // Initialize Crafty
        Crafty.init(1080, 600, $('#game')[0]).background('cyan');

        Crafty.scene('Loading');

        // Load Tile Map
        Crafty.sprite(128, "images/tilemap.png", {Grass:[0,0,1,1], Stone:[0,1,1,1], GrassS:[1,0,1,1], StoneS:[1,1,1,1]});

        // Load Character
        Crafty.sprite(45, 53, "images/player.png", {Player:[0,0,1,1]});

        var iso = Crafty.isometric.size(128,74);    // top of tile ends 74.213px down from top of sprite

        //var player = Crafty.e('2D, Canvas, Player').attr({z:1});

        var tiles = createArray(Game.tile_grid.width, Game.tile_grid.height);
/*
        Crafty.c('Grass', {
            required: '2D, Canvas, Mouse, spr_grass'
        });*/

        /*var tile = Crafty.e('2D, Canvas, Grass, Mouse');
        console.log(tile);
        var tile2 = Crafty.e('Grass');
        console.log(tile2);*/

        /*for (var y=0; y<Game.tile_grid.height; y++) {
            for (var x=0; x<Game.tile_grid.width; x++) {
                var which = Crafty.math.randomInt(0,1);
                var tile = Crafty.e('2D, Canvas, ' + (!which ? "Grass" : "Stone") + ', Mouse')
                .areaMap(0, 38, 64, 0, 128, 38, 64, 76)
                .bind("Click", function(e) {
                    player.x = this.x + (this.w / 2 - (player.w / 2));
                    player.y = this.y;

                    var pos = iso.px2pos(this.x, this.y);
                    //console.log(pos);

                    //console.log(this);
                    //console.log(tiles[pos.y][pos.x]);

                    for (var j=pos.y-1; j<=pos.y+1; j++)
                        for (var i=pos.x-1; i<=pos.x+1; i++)
                            tiles[j][i].sprite(1,0,1,1);

                }).bind("MouseOver", function(e) {
                    if (this.has("Grass"))
                        this.sprite(1,0,1,1);
                    else
                        this.sprite(1,1,1,1);
                }).bind("MouseOut", function(e) {
                    if (this.has("Grass"))
                        this.sprite(0,0,1,1);
                    else
                        this.sprite(0,1,1,1);
                });



                iso.place(x, y, 0, tile);

                tiles[y][x] = tile;
            }
        }*/
        //console.log(tiles);
    }
}

// When the DOM has loaded
$(document).ready(function() {
    Game.start();
});

function createArray(length) {
    var arr = new Array(length || 0),
        i = length;

    if (arguments.length > 1) {
        var args = Array.prototype.slice.call(arguments, 1);
        while(i--) arr[length-1 - i] = createArray.apply(this, args);
    }

    return arr;
}

$text_css = { 'font-size': '24px', 'font-family': 'Arial', 'color': 'white', 'text-align': 'center' }
