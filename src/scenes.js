/* Main Game Scene */
Crafty.scene('Game', function() {
    //var tiles = createArray(Game.tile_grid.width, Game.tile_grid.height);

    var player = Crafty.e('PlayerCharacter').at(1,1);

    var iso = Crafty.isometric.size(128,74);    // top of tile ends 74.213px down from top of sprite

    //Crafty.sprite(128, "images/tilemap.png", {spr_grass:[0,0,1,1], Stone:[0,1,1,1], GrassS:[1,0,1,1], StoneS:[1,1,1,1]});

    Crafty.c('Grass', {
        required: '2D, Canvas, Mouse, spr_grass'
    });

    var tile = Crafty.e('Grass');

    console.log(tile);

    iso.place(1,1,0,tile);

    for (var y=0; y<Game.tile_grid.height; y++) {
        for (var x=0; x<Game.tile_grid.width; x++) {
            var tile = Crafty.e('Grass')
            .areaMap(0, 38, 64, 0, 128, 38, 64, 76)
            /*.bind("Click", function(e) {
                player.x = this.x + (this.w / 2 - (player.w / 2));
                player.y = this.y;

                var pos = iso.px2pos(this.x, this.y);
                //console.log(pos);

                //console.log(this);
                //console.log(tiles[pos.y][pos.x]);

                for (var j=pos.y-1; j<=pos.y+1; j++)
                    for (var i=pos.x-1; i<=pos.x+1; i++)
                        tiles[j][i].sprite(1,0,1,1);
            });*/
            ;

            iso.place(x, y, 0, tile);
            console.log('placing');
        }
    }

    //console.log(tiles);
});

/* Loading Scene */
Crafty.scene('Loading', function() {
    Crafty.e('2D, DOM, Text')
        .text('Loading... Please Wait')
        .attr({ x: 0, y: Game.height() / 2 - 24, w: Game.width() })
        .textFont($text_css);

    Crafty.load({
        "images": [
            'images/tilemap.png',
            'images/player.png'
        ]
    }, function() {
        // once the images are loaded
        Crafty.sprite(45, 53, "images/player.png", {spr_player:[0,0,1,1]});
        Crafty.sprite(128, "images/tilemap.png", {spr_grass:[0,0,1,1], spr_stone:[0,1,1,1], spr_grass_s:[1,0,1,1], spr_stone_s:[1,1,1,1]});
    });

    // start the game
    Crafty.scene('Game');
});
